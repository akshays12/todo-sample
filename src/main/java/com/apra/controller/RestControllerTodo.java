package com.apra.controller;
import java.net.MalformedURLException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.UpdateConflictException;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.apra.TodoView.TodoCouchView;
import com.apra.controller.CouchDBManager;

@CrossOrigin
@RestController
@RequestMapping("todo")
public class RestControllerTodo {
	@Value("${app.db_url}")
	private String DB_URL;
	@Value("${app.db_user}")
	private String DB_USER;
	@Value("${app.db_password}")
	private String DB_PWD;
	@Value("${app.db_remote}")
	private String REMOTE_DB;
	
	
	
	private int version;
	private TodoCouchView couchView;
	private CouchDBManager couchDBManager;
	
	public RestControllerTodo() {
		// TODO Auto-generated constructor stub
		version = 1;
	}
	
	@PostConstruct
	public void postInitSetup()
	{
		couchDBManager = new CouchDBManager(DB_URL,DB_USER,DB_PWD,REMOTE_DB);
		couchDBManager.openDB();
		couchView = new TodoCouchView(couchDBManager.db);
		
	}
	
	/**
	 * This is a GET API that checks version sent by client.
	 * 
	 * @return: Map with version true/false and 200 OK
	 *          
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/checkClientVersion")
	public ResponseEntity<LinkedHashMap<String, Object>> checkClientVersion(
			@RequestParam("version") String version) {
		
		LinkedHashMap<String, Object> map = new LinkedHashMap<>();
		if(this.version == Integer.parseInt(version) ) {
			map.put("version", true);
		}else {
			map.put("version", false);
		}
		return new ResponseEntity<LinkedHashMap<String, Object>>(map, HttpStatus.OK);
		
	}
	
	/**
	 * This is a POST API saves device name and info to DB
	 * 
	 * @return: Map with success true/false and 200 OK
	 *          
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/saveDeviceNameInfo")
	public ResponseEntity<LinkedHashMap<String, Object>> saveDeviceNameInfo(
			@RequestBody Map<String, String> payload) {
		
		LinkedHashMap<String, Object> map = new LinkedHashMap<>();
		DeviceNameInfo deviceNameInfo = new DeviceNameInfo(payload);
		couchView.saveDeviceNameInfo(deviceNameInfo);
		return new ResponseEntity<LinkedHashMap<String, Object>>(map, HttpStatus.OK);
		
	}
	
	/**
	 * This is a GET API gets all device name and info from DB
	 * 
	 * @return: Map with device name and info
	 *          
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/getDeviceNameInfo")
	public ResponseEntity<LinkedHashMap<String, Object>> getDeviceNameInfo() {
		LinkedHashMap<String, Object> map = new LinkedHashMap<>();
		map.put("devices", couchView.getDeviceNameInfo());
		return new ResponseEntity<LinkedHashMap<String, Object>>(map, HttpStatus.OK);
		
	}
	
	
}
