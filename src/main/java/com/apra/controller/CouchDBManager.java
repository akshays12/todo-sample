package com.apra.controller;

import java.net.MalformedURLException;

import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.UpdateConflictException;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.apra.TodoView.TodoCouchView;


public class CouchDBManager {
	private String DB_URL;
	private String DB_USER;
	private String DB_PWD;
	private String REMOTE_DB;
	

	public CouchDbInstance dbInstance = null;
	public CouchDbConnector db = null;

	public CouchDBManager(String DB_URL,String DB_USER,String DB_PWD,String REMOTE_DB) {
		this.DB_URL = DB_URL;
		this.DB_USER = DB_USER;
		this.DB_PWD = DB_PWD;
		this.REMOTE_DB = REMOTE_DB;
	}
	
	public CouchDbInstance openDB(CouchDbInstance dbInstance) {
		HttpClient httpClient;
		try {
			httpClient = new StdHttpClient.Builder().url(DB_URL).username(DB_USER).password(DB_PWD).build();
			dbInstance = new StdCouchDbInstance(httpClient);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
		return dbInstance;
	}
	
	public boolean openDB() {
		if (dbInstance == null) {
			dbInstance = openDB(dbInstance);
			if (dbInstance == null)
				return false;
			
			db = new StdCouchDbConnector(REMOTE_DB, dbInstance);
			db.createDatabaseIfNotExists();
		}

		return true;
	}
}
