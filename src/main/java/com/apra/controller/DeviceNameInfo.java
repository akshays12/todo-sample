package com.apra.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.apra.TodoApplication;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeviceNameInfo {
	
	@JsonProperty("_id") public String id;
    @JsonProperty("_rev") public String revision;
    @JsonProperty("name") public String name;

	public String type= "DeviceNames";
    @JsonProperty("deviceNameInfo")
    public HashMap<String, String> deviceNameInfo = null;

    public DeviceNameInfo(Map<String, String> deviceNameInfo) {
		setDeviceNameInfo(deviceNameInfo);
    }
    
    public DeviceNameInfo() {
		deviceNameInfo = new HashMap<String, String>();
//		id=UtilsIPRD.getGUID();
    }
    
    public HashMap<String, String> getDeviceNameInfo() {
		return deviceNameInfo;
	}

	public void setDeviceNameInfo(Map<String, String> deviceNameInfoMap) {
		this.name = deviceNameInfoMap.get("name");
	}
	
	private String getmDevId() {
		// TODO Auto-generated method stub
		return this.id;
	}
	
}
